#include <stdio.h>
#include <stdlib.h>
#define N 15

/* función punto de entrada */
int main(){


 
    int emento[N];

    /* Condiciones de contorno*/
    emento[1] = emento[0] = 1;
  
    /* calculos*/
    for (int i=2; i<N; i++)
        emento[i] = emento[i-1] + emento[i-2];
    
    /* salida de datos*/
    for (int i=0; i<N; i++)
        printf("%i ", emento[i]);

    for (int i=1; i<N; i++)
        printf("%.4lf ", (double) emento[i] / emento[i-1]);
    

    printf("\n");
	return EXIT_SUCCESS;
}	
