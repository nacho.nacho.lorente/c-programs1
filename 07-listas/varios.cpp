#include <stdio.h>
#include <stdlib.h>


/* Función punto de entrada */
int main(){

    unsigned int  primo[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 27};
    unsigned elementos = (unsigned) sizeof(primo) / sizeof(int);
    unsigned *peeping = primo;
    char *tom = (char *) primo;
    /* %u es de unsigned */
    /* %l es para que ocupe el doble de 4 a 8*/    
    printf(" PRIMO:\n"
            " ======\n"
            " Localizacion (%p)\n"
            " Elementos: %u [%u .. %u]\n"
            " Tamaño: %lu bytes.\n",
            primo,
            elementos,
            primo[0], primo[elementos-1],
            sizeof(primo));
    /* EL * ES ALLI DONDE APUNTO LO CONTRARIO AL & */ 
    printf( "0: %u\n", peeping[0] );
    printf( "1: %u\n", peeping[1] );
    printf( "0: %u\n", *peeping );
    printf( "1: %u\n", *(peeping+1) );
    printf( "Tamaño: %lu bytes. \n", sizeof(peeping) );
    printf( "\n");

    /* Memory Dump = Volcado de memoria */
    for (int i = 0; i<sizeof(primo); i++)
        printf("%02X", *(tom +i));

    printf( "\n\n" );

    return EXIT_SUCCESS;
}	
