#include <stdio.h>
#include <stdlib.h>

#define BMP_NAME "fary.bmp"
#define WIDTH 270L
#define HEIGHT 270L
#define Bpp   3
#define ROW_SIZE (WIDTH * Bpp + 3 ) / 4 * 4

#define IMAGE_OFFSET 0x8A
//#define IMAGE_OFFSET 0x36

int  main(int argc, char *argv[]){
    unsigned char *image;
    image = (unsigned char *) malloc( HEIGHT * ROW_SIZE );


    FILE *pf;
    if (!(pf = fopen (BMP_NAME, "r"))){
        fprintf (stderr, "Mac donde estas?\nRai no te veo.\n");
        return EXIT_FAILURE;
    }
    fseek (pf, IMAGE_OFFSET, SEEK_SET);
    fread (image, 1, HEIGHT * ROW_SIZE, pf);
    fclose (pf);

    for (int row=HEIGHT; row>=0; row--) {
        for (int col=0; col<WIDTH; col++) {
            double media = 0;
            for (int i=0; i<Bpp; i++)
                media += image[ROW_SIZE * row + Bpp * col + i];
            media /= 3;
            if (media < 50)
                printf ("█");
            else if (media <100)
                printf ("▓");
            else if (media <150)
                printf ("▒");
            else if (media <200)
                printf ("░");
            else
                printf (" ");
        }
        printf ("\n");
    }

    free (image);
    return EXIT_SUCCESS;
}

