#include <stdio.h>
#include <stdlib.h>

#define N 15
#define DUMP "fibonaccijoputa.dat"

/* función punto de entrada */
int main(void){
    int nuevo = 1, viejo = 0;

    int c [N];

    for(int i=0; i<N; i++){
        //printf("%d\n",b);
        //printf("%d\n",a);
        c[i] = nuevo + viejo;
        viejo = nuevo;
        nuevo = c[i];
        printf("%i\n", c[i]);
    }

    FILE *pf;
    if ( !(pf = fopen (DUMP, "wb")) ){
        fprintf (stderr, "BocataJamon.\n");
        return EXIT_FAILURE;
    }

    fwrite (c, sizeof (int), N, pf);

    fclose (pf);

    return EXIT_SUCCESS;
}	
