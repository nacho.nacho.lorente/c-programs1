#include "stdio.h"
#include "stdlib.h"
int main(){

    int tart[15];
    int n=15;
    int x, i, j; // i=fila  j=columna

    x=0;

    //valida el numero de lineas
    for (i=1; i<=n ; i++)
    {
        //Construimos el triangulo de pascal
        for (j=x; j>=0; j--)
        {
            if(j==x || j==0)
            {
                tart[j] = 1;
            }
            else
            {
                tart[j] = tart[j] + tart[j-1];

            }
        }

        x++;
        printf("\n");
        //Truco para imprimir el triangulo
        for (j=1; j<=n-i; j++) 
            printf("   ");

        for(j=0; j<x; j++)
        {
            printf("%3d   ", tart[j]);
            
            
        }
        printf("\n");
    }
    return 0;
}
