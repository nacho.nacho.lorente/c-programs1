#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 0x100

int main (int argc, char *argv[]) {

    
    char buffer[N];
    int len;
    int p;
    char **palabra = NULL;

    for(p=0; strcmp(buffer, ".\n"); p++) {
        
        palabra = (char **) realloc (palabra, (p+1) * sizeof(char *));
        printf("Palabra pensada por el usuario: ");
        fgets(buffer, N-1, stdin);

        len = strlen (buffer);
        palabra[p] = (char *) realloc(palabra[p], len);
        strncpy (palabra[p], buffer, len);
        palabra[p][len-1] = '\0'; 
    }

    free(palabra[p-1]);
    palabra[p-1] = NULL;

    for(int l=0; palabra[l]; l++)
        printf("\t%s\n", palabra[l]);

    for(int l=0; palabra[l]; l++)
        free (palabra[l]);

    free (palabra);

    return EXIT_SUCCESS;
}

