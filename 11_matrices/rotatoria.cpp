#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define DIMpf 1
#define DIMpc 2
#define DIMmf 2
#define DIMmc 2
#define CONrg 3.14159265 / 180

int main(){

    /* Entrada de Datos */

    double angulo;
    double punto[DIMpf][DIMpc] = {
        {1, 2}},
           matrizF[DIMpf][DIMmc];

    printf("Dime el punto: ");
    scanf(" %lf %lf", &punto[0][0], &punto[0][1]);

    printf("Dime el angulo de giro: ");
    scanf(" %lf", &angulo);

    double matriz[DIMmf][DIMmc] = {
        {cos(angulo * CONrg), -sin(angulo * CONrg)},
        {sin(angulo * CONrg), cos(angulo * CONrg)}};

    /* Calculos */

    for(int i=0; i<DIMpf; i++)
        for(int j=0; j<DIMmc; j++)
            for(int z=0; z<DIMpc; z++)
                matrizF[i][j] += punto[i][z] * matriz[z][j];

    /* Salida de Datos */

    printf("Tu nuevo punto esta en %.2lfx %.2lfy\n",matrizF[0][0], matrizF[0][1]);

    return EXIT_SUCCESS;
}

