#include <stdio.h>
#include <stdlib.h>

#define DIMmf 2
#define DIMmc 3
#define DIMvf 3
#define DIMvc 1

int main(){

    double matriz[DIMmf][DIMmc],
    vector[DIMvf][DIMvc],
    matrizF[DIMmf][DIMvc];

    for(int i=0; i<DIMmf; i++){
        printf("Dime los valores de la fila %i de la matriz: ", i);
        scanf(" %lf, %lf, %lf", &matriz[i][0], &matriz[i][1], &matriz[i][2]);
    }

    printf("Dime los valores del vector: ");
    scanf(" %lf, %lf, %lf", &vector[0][0], &vector[1][0], &vector[2][0]);

    if(DIMmc != DIMvf){
        printf("No se puede multiplicar\n");
        return 0;
    }

    for(int i=0; i<DIMmf; i++)
        for(int j=0; j<DIMmc; j++)
            matrizF[0][i] += matriz[i][j] * vector[j][0];

    printf(" %.2lf\n %.2lf\n", matrizF[0][0], matrizF[0][1]);

    return EXIT_SUCCESS;
}


