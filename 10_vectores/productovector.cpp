#include <stdio.h>
#include <stdlib.h>
#define D 3

/* función punto de entrada */
int main(){
    double a[3][3]={
        {2,3,2},
        {7,5,4},
        {4,8,2}
    };
      double determinante = 0,
             multiplicacion;

      for(int f=0; f<D; f++){
          multiplicacion = 1;
          for (int d=0; d<D; d++)
              multiplicacion *= a[(f+d)%D][0+d];
          determinante += multiplicacion;
      }

      for(int f=0; f<D; f++){
          multiplicacion = 1;
          for (int d=0; d<D; d++)
              multiplicacion *= a[(f+d)%D][2-(0+d)];
          determinante -= multiplicacion;
      }



      printf("Determinante: %.2lf\n", determinante);


	return EXIT_SUCCESS;
}	
