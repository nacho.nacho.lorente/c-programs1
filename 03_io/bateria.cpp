


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define VECES 13

#define BTU 100000 /* Basic Time Unit */

/* Funcion punto de entrada */
int main(){
	int duracion [VECES] = {0, 2, 8, 2, 1, 8, 2, 1, 2, 1, 2, 2, 2 };

	
	for (int i=0; i<VECES; i++) {	
	fputc('\a', stderr);
	usleep(duracion[i] * BTU);

	}

	return 0;
}	
