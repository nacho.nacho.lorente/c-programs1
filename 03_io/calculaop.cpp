#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Declaracion de variables */

int main()  {
    
    int i, 
    int numero;

    /* Titulo */
    system("toilet -fpagga --metal Tabla de multiplicar: ");

    /* Pregunta del programa */
    printf("Dime numero que quieres multiplicar:  ");

    /* Calculos */
    scanf(" %d", &numero);
    for (int i=0; i<=10; i++)
         printf(" %d * %d = %d \n", i, numero, i * numero);

    return EXIT_SUCCESS;
}
