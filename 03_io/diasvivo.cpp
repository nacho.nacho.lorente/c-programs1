#include <stdio.h>
#include <stdlib.h>

/* función punto de entrada */
int main (){
	int dias , meses, annios;	
	int total_dias=0;
	//considero que año es 365 dias y mes 30 dias
	printf("Decir años que tienes: ");
	scanf("%i", &annios);
	printf("Decir mes en el que naciste: ");
	scanf("%i", &meses);	
	printf("Decir dia en el que naciste: ");
	scanf("%i", &dias);
	annios=(annios*365);
	meses=(meses*12);
	total_dias=(dias + meses + annios);
	printf("Total de dias vividos: %i\n", total_dias);

	return EXIT_SUCCESS;
}	
