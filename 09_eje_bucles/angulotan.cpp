#include <stdio.h>
#include <stdlib.h>
#include <math.h>


/* función punto de entrada */
int main(){

    double vector[2], angulo;

    printf("vector: ");
    scanf(" %lf, %lf", &vector[0], &vector[1]);

    angulo = atan2(vector[1], vector[0]);
   printf("Angulo por pantalla: %lf \n", angulo * 180/M_PI);  
	return EXIT_SUCCESS;
}	
