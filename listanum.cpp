#include <stdio.h>
#include <stdlib.h>

#define N 10

/* función punto de entrada */
int main(){

    unsigned lista[N];

    for (int i=0; i<N; i++)
        printf ("lista[%i] = %u\n", i, lista[i]); //ver el valor de i

    printf("\n");

	return EXIT_SUCCESS;
}	
