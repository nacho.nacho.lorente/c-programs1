#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>

#include "snake.h"


void pintar (struct TSnake snake){
    clear();
    for (int i=0; i<snake.cima; i++)
          mvprintw ( snake.anillo[i].pos.y,
                     snake.anillo[i].pos.x, "0" );
    refresh ();
}

int main(int argc, char *argv[]){
    struct TSnake snake;
    int input;

    initscr ();
    halfdelay (2);
    keypad (stdscr, TRUE);
    iniciar (LINES, COLS);
    curs_set (0);
    parir (&snake);
    
    do {
       input = getch ();
      if (input >= KEY_DOWN && input <= KEY_RIGHT)  
        snake.anillo[0].vel = velocidades[input - KEY_DOWN];
            
       mover (&snake);  //mover el snake
       if (snake.anillo[0].pos.x > COLS)
           snake.anillo[0].pos.x = 0;

       if  (snake.anillo[0].pos.y > LINES)
           snake.anillo[0].pos.y = 0;

      
       
      

       

       pintar (snake);// pintar snake
    } while ( input != 0x1B );
     
    curs_set (1);
    endwin();

	return EXIT_SUCCESS;
}	
