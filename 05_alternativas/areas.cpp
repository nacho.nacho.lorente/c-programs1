#include <stdio.h>
#include <stdlib.h>

int main(){
    int resultado;  
    int a;
    int b;    
    int opcion; 
    
    printf("Esto es un programa que te permitirá hacer el área del cuadrado(1), del rectángulo(2) o del rombo(3).\n");
    printf("Indique el número de la operación que quieres utilizar: ");
    scanf("%i", &opcion);
    printf("Usted ha elegido %i\n", opcion);

    switch(opcion)
    {
        case 1:
            printf("\n -> Introduzca el valor del lado: ");
            scanf("%i", &a);
            resultado=a*a;
            printf("\nEl área del cuadrado es: %i\n", resultado);
            break;

        case 2:
            printf("\n -> Introduzca el valor de la altura: ");
            scanf("%i", &a);
            printf("\n -> Introduzca el valor de la base: ");
            scanf("%i", &b);
            resultado=a*b;
            printf("\nEl área del rectángulo es: %i\n", resultado);
            break;

        case 3:
            printf("\n -> Introduzca el valor de la diagonal mayor: ");
            scanf("%i", &a);
            printf("\n -> Introduzca el valor de la diagonal menor: ");
            scanf("%i", &b);
            resultado=(a*b)/2;
            printf("\nEl área del rombo es: %i\n", resultado);
            break;

    }

    return EXIT_SUCCESS;
}

