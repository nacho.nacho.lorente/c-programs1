#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX 10
#define INC /*incremento*/ 0.001

/* función punto de entrada */
int main(){
  
  double f/* polinomio*/ = 0, potencia, x, ultimo=0;
  

  double c[MAX] = {3 ,1 ,0, 0, 0, 0, 0, 0, 0, 0,};

  printf ("Limite Inferior: ");
  scanf (" %lf", &x);


  /* Pasos para sacar el algoritmo
  1·x^0 + 2·x^1 + 3·x^2 + 4·x^3 + 5·x^4
  c[0]·x^0 + c[1]·x^1 + c[2]·x^2 + c[3]·x^3 + c[4]·x^4
  c[i]·x^i
  f += c[i]·x^i*/



  /* Opcion 1*/
  for (double t=x; ultimo*f>=0 ;t+=INC){
      ultimo = f;
  for (int termino = 0; termino<MAX; termino++){
      potencia = 1;
  for (int exponente=0; exponente<potencia; exponente++)
      potencia *= x; 
  f += c[termino] * potencia; /*pow(x, termino);*/
  }
  printf("f(%.2lf) = %2lf\n", x, f);
  getchar();
  }

  /*
  for (int vez=0; vez<potencia; vez++)
      exponente *=x;*/


  /* alternativa
  pow(x,potencia);*/

	return EXIT_SUCCESS;
}	
