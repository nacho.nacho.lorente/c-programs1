var width=1300
var height=800
var XC = width / 2
var YC = height / 2
var r1, r2
var clear
var alpha = 0
var R = []
var ctx = null

/*
 *  * n: number of points
 *   * r1: inner radius
 *    * r2: outer radius
 *     * */
function star(n,r1, r2){
    var p = []
    var r = [r1, r2]
    var delta = Math.PI / n
    for (i=0; i<2*n; i++)
        p.push([r[i%2] * Math.cos(i * delta), r[i%2] * Math.sin(i * delta)])

    return p
}

/*
 *  * Converts world coordinates to screen ones
 *   * p: A single point specified as an array.
 *    */
function s(p){
    return [XC+p[0], YC-p[1]]
}
/*
 * var R = [[cos, sen],
 *          [-sen, cos]]
 *                   */
/* Draw a list of points
 *  * p: Array of points
 *   */
function traza(p){
    var c
    ctx.beginPath()
    c = s(p[0])
    ctx.moveTo(c[0], c[1])
    for (var i=1; i<p.length; i++){
        c = s(p[i])
        ctx.lineTo(c[0], c[1])
    }
    c = s(p[0])
    ctx.lineTo(c[0], c[1])
    ctx.stroke()
}

function borra(){
    ctx.clearRect(0, 0, width, height)
}

/*
 *  * Multiplica una lista de puntos por una matriz
 *   */
function multiplica(m, l){
    var g = [] // Puntos girados
    for (var p=0; p<l.length; p++) //Para cada uno de los puntos
        g[p] = [ l[p][0] * Math.cos(alpha) - l[p][1] * Math.sin(alpha),
            l[p][0] * Math.sin(alpha) + l[p][1] * Math.cos(alpha),
        ]
    return g
}

function reload(){
    clear = document.getElementById("clear").checked
    r1 = parseInt(document.getElementById("radio1").value)
    r2 = parseInt(document.getElementById("radio2").value)
    alpha = parseInt(document.getElementById("angle").value) * Math.PI / 180
    R = [[Math.cos(alpha), Math.sin(alpha)],
        [-Math.sin(alpha), Math.cos(alpha)]]
    if (clear)
        borra();
    traza(multiplica(R, star(5, r1, r2)))
}

function main() {
    ctx = document.getElementById("lienzo").getContext("2d")
    reload()
}

